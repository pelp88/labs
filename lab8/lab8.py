import matplotlib.pyplot as plt
import matplotlib.patches as pch
import pelp88_stuff
import random


n = int(input('Enter quantity of points: '))
rad = float(input('Enter radius: '))

points = [(random.uniform(-10, 10), random.uniform(-10, 10)) for i in range(n)]
center = (random.uniform(-10, 10), random.uniform(-10, 10))

points_found_counter = pelp88_stuff.point_counter(points, center, rad)

print(f'Points found: {points_found_counter}')

ax = plt.subplot()
ax.scatter(center[0], center[1], c='red', s=11, zorder=100, marker='X')
ax.scatter([x for x, y in points if pelp88_stuff.is_point_in_area((x, y), center, rad) is True],
           [y for x, y in points if pelp88_stuff.is_point_in_area((x, y), center, rad) is True],
           s=9,
           c='green',
           zorder=50,
           marker='p')
ax.scatter([x for x, y in points if pelp88_stuff.is_point_in_area((x, y), center, rad) is False],
           [y for x, y in points if pelp88_stuff.is_point_in_area((x, y), center, rad) is False],
           s=3,
           c='black',
           zorder=0)

ax.add_patch(pch.Circle(center, radius=rad, fill=False))
ax.axis('square')
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)
plt.show()

