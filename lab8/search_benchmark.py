import time
import pelp88_stuff
import numpy as np
import matplotlib.pyplot as plt
import random

n_min = 10e4
n_max = 10e5
n_step = 10e4

n_data = []
time_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    points = [(random.uniform(-10, 10), random.uniform(-10, 10)) for i in range(n)]
    center = (random.uniform(-10, 10), random.uniform(-10, 10))
    rad = n / 4
    for _ in range(3):
        start = time.time()
        temp = pelp88_stuff.point_counter(points, center, rad)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_data.append(n)
    time_data.append(average)

plt.plot(n_data, time_data, 'o')
trend = np.poly1d(np.polyfit(n_data, time_data, 1))
plt.plot(n_data, trend(n_data), 'b', c='red', label=f'Функция тренда - {trend}')
plt.legend()
plt.xlabel('кол-во точек')
plt.ylabel('время выполнения, с.')

plt.show()
