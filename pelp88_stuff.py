from math import sinh, atanh


def g_func(x, a):
    return 5 * (10 * a ** 2 - 11 * a * x + x ** 2) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)


def f_func(x, a):
    return sinh(3 * a ** 2 + 7 * a * x + 4 * x ** 2)


def y_func(x, a):
    return atanh(30 * a ** 2 + 37 * a * x - 4 * x ** 2)


def is_point_in_area(point, center, rad):
    return (float(center[0]) - float(point[0])) ** 2 + (float(center[1]) - float(point[1])) ** 2 <= rad ** 2


def point_counter(points, center, rad):
    points_counter = 0
    for point in points:
        if is_point_in_area(point, center, rad):
            points_counter += 1
    return points_counter
