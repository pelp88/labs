import pelp88_stuff

choice = int(input('Which function you wanna execute (G, F, Y)?: '))
variables = [float(input("Enter X: ")), float(input("Enter A: "))]

if choice == 'G':
    try:
        print(f"G = {pelp88_stuff.g_func(variables[0], variables[1]):.3f}")
    except ValueError:
        print("You gave wrong values for chosen function")
elif choice == 'F':
    try:
        print(f"F = {pelp88_stuff.f_func(variables[0], variables[1]):.3f}")
    except ValueError:
        print("You gave wrong values for chosen function")
elif choice == 'Y':
    try:
        print(f"Y = {pelp88_stuff.y_func(variables[0], variables[1]):.3f}")
    except ValueError:
        print("You gave wrong values for chosen function")
