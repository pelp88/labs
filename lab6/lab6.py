from numpy import arange
from pelp88_stuff import g_func, f_func, y_func

print("Ctrl-C to terminate program")

while True:

    variables1 = list(map(float, input("Enter X low and high point, A argument and step for G: ").split()))
    variables2 = list(map(float, input("Enter X low and high point, A argument and step for F: ").split()))
    variables3 = list(map(float, input("Enter X low and high point, A argument and step for Y: ").split()))

    function_data = [[], [], []]

    try:
        for i in arange(variables1[0], variables1[1], variables1[3]):
            try:
                function_data[0].append(g_func(i, variables1[2]))
            except ValueError or OverflowError:
                function_data[0].append(None)
        for i in arange(variables2[0], variables2[1], variables2[3]):
            try:
                function_data[1].append(f_func(i, variables2[2]))
            except ValueError or OverflowError:
                function_data[1].append(None)
        for i in arange(variables3[0], variables3[1], variables3[3]):
            try:
                function_data[2].append(y_func(i, variables3[2]))
            except ValueError or OverflowError:
                function_data[2].append(None)
    except KeyboardInterrupt:
        print('Program terminated')

    [print(f"G: f(x) = {function_data[0][x]}") for x in range(len(function_data[0]))]
    [print(f"F: f(x) = {function_data[1][x]}") for x in range(len(function_data[1]))]
    [print(f"Y: f(x) = {function_data[2][x]}") for x in range(len(function_data[2]))]

    exit_choice = input("Do you want to run the program again? - (y/n)")
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Wrong choice, program terminated.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()
