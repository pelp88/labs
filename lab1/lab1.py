import pelp88_stuff

x = float(input('Enter x for G function: '))
a = float(input('Enter a for G function: '))  # input for G
print("G = ", float("{}".format(pelp88_stuff.g_func(x, a))))

x = float(input('Enter x for F function: '))
a = float(input('Enter a for F function: '))  # input for F
print("F = ", float("{}".format(pelp88_stuff.f_func(x, a))))  # F calculation and output

x = float(input('Enter x fot Y function: '))
a = float(input('Enter a for Y function: '))  # input for Y
print("Y = ", float("{}".format(pelp88_stuff.y_func(x, a))))  # Y calculation and output
