import pelp88_stuff

print("Ctrl-C to terminate program")

while True:

    choice = input("Which function you wanna execute (G, F, Y)?: ")
    variables = list(map(float, input("Enter X lower and higher point, A argument and step: ").split()))

    function_data = []

    x = variables[0]
    while x < variables[1]:
        try:
            if choice == 'G':
                try:
                    print(f"x = {x:.3f} | f(x) = {pelp88_stuff.g_func(x, variables[2]):.3f}")
                    function_data.append(pelp88_stuff.g_func(x, variables[2]))
                except ValueError or OverflowError:
                    print("You gave wrong values for chosen function")
                    function_data.append(None)
            elif choice == 'F':
                try:
                    print(f"x = {x:.3f} | f(x) = {pelp88_stuff.f_func(x, variables[2]):.3f}")
                    function_data.append(pelp88_stuff.f_func(x, variables[2]))
                except ValueError or OverflowError:
                    print("You gave wrong values for chosen function")
                    function_data.append(None)
            elif choice == 'Y':
                try:
                    print(f"x = {x:.3f} | f(x) = {pelp88_stuff.y_func(x, variables[2]):.3f}")
                    function_data.append(pelp88_stuff.y_func(x, variables[2]))
                except ValueError or OverflowError:
                    print("You gave wrong values for chosen function")
                    function_data.append(None)
            x += variables[3]
        except KeyboardInterrupt:
            print('Program terminated')

    if len(function_data) == 1:
        if None in function_data:
            print("No values given are correct")
        else:
            print(f"Min value = max value = {list(function_data)[0]:.3f}")
    elif None in function_data:
        function_data.pop(None)
        print(f"Min value = {sorted(function_data)[0]} | Max value = {sorted(function_data)[-1]}")
    else:
        print(f"Min value = {sorted(function_data)[0]} | Max value = {sorted(function_data)[-1]}")

    exit_choice = input("Do you want to run the program again? - (y/n)")
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Wrong choice, program terminated.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()
