import matplotlib.pyplot as plt
import pelp88_stuff


print('Ctrl-C to terminate program')

while True:

    choice = input('Which function you wanna execute (G, F, Y)?: ')
    variables = list(map(float, input("Enter X lower and higher point, A argument and step: ").split()))

    x_list, y_list = [], []

    x = variables[0]
    while x < variables[1]:
        try:
            x_list.append(x)
            if choice == 'G':
                try:
                    print(f'x = {x:.3f} | f(x) = {pelp88_stuff.g_func(x, variables[2]):.3f}')
                    y_list.append(pelp88_stuff.g_func(x, variables[2]))
                except ValueError:
                    print("You gave wrong values for chosen function")
                    y_list.append(None)
            elif choice == 'F':
                try:
                    print(f'x = {x:.3f} | f(x) = {pelp88_stuff.f_func(x, variables[2]):.3f}')
                    y_list.append(pelp88_stuff.f_func(x, variables[2]))
                except ValueError:
                    print("You gave wrong values for chosen function")
                    y_list.append(None)
            elif choice == 'Y':
                try:
                    print(f'x = {x:.3f} | f(x) = {pelp88_stuff.y_func(x, variables[2]):.3f}')
                    y_list.append(pelp88_stuff.y_func(x, variables[2]))
                except ValueError:
                    print("You gave wrong values for chosen function")
                    y_list.append(None)
            x += variables[3]
        except KeyboardInterrupt:
            print('Program terminated')

    plt.plot(x_list, y_list)
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.show()

    exit_choice = input("Do you want to run the program again? - (y/n)")
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Wrong choice, program terminated.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()