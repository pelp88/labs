from pelp88_stuff import g_func, f_func, y_func

print("Ctrl-C to terminate program")

while True:

    choice = input("Which function you wanna execute (G, F, Y)?: ")
    variables = list(map(float, input("Enter X low and high point, A argument and step: ").split()))
    template = input("Enter template: ")

    function_data = []

    x = variables[0]
    while x < variables[1]:
        try:
            if choice == 'G':
                try:
                    function_data.append(g_func(x, variables[2]))
                except ValueError or OverflowError:
                    function_data.append(None)
            elif choice == 'F':
                try:
                    function_data.append(f_func(x, variables[2]))
                except ValueError or OverflowError:
                    function_data.append(None)
            elif choice == 'Y':
                try:
                    function_data.append(y_func(x, variables[2]))
                except ValueError or OverflowError:
                    function_data.append(None)
            x += variables[3]
        except KeyboardInterrupt:
            print('Program terminated')

    print(f"Answer is = {', '.join([str(x) for x in function_data])}")
    print(f"The template met {len([1 for x in function_data if str(x) == template])} times.")

    exit_choice = input("Do you want to run the program again? - (y/n)")
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Wrong choice, program terminated.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()
